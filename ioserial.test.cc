/* Copyright 2020-2022 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of ioserial.
 *
 * ioserial is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * Ioserial is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ioserial.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "ioserial.hh"

using namespace std;
using namespace ioserial;

string dev0, dev1;

void test_baudrates()
{
    {
        serialbuf buf0, buf1;
        buf0.open(dev0, ios_base::in | ios_base::out);
        buf1.open(dev1, ios_base::in | ios_base::out);
        if (buf0.baudrate() != 9600 || buf1.baudrate() != 9600)
            throw runtime_error( "Default baudrate is not 9600bps but "
                    + to_string(buf0.baudrate()) + "bps");
    }
    cerr << "Testing baudrates (please be patient) ..." << endl;
#ifdef IOSERIAL_POSIX
    vector<unsigned> baudrate_list = { 50, 75, 110, 134, 150, 300, 600, 1200,
        1800, 2400, 4800, 9600, 19200, 38400, 57600, 115200, 230400 };
#elif defined IOSERIAL_WINDOWS
    vector<unsigned> baudrate_list = { 110, 300, 600, 1200, 2400, 4800, 9600,
        14400, 19200, 38400, 57600, 115200, 128000, 256000 };
#endif
    std::reverse(baudrate_list.begin(), baudrate_list.end());
    for (auto baudrate : baudrate_list)
    {
        serialbuf buf0, buf1;
        buf0.open(dev0, ios_base::in | ios_base::out);
        buf1.open(dev1, ios_base::in | ios_base::out);
        buf0.baudrate(baudrate);
        buf1.baudrate(baudrate);
        if (buf0.baudrate() != baudrate)
            cerr << "Speed mismatch: Set " + to_string(baudrate)
                  + " in device 0 bps but got "
                  + to_string(buf0.baudrate()) + "bps" << endl;
        if (buf1.baudrate() != baudrate)
            cerr << "Speed mismatch: Set " + to_string(baudrate)
                  + " in device 1 bps but got "
                  + to_string(buf1.baudrate()) + "bps" << endl;

        int count_to;
        if (baudrate <= 110)
            count_to = 2;
        else if (baudrate <= 600)
            count_to = 8;
        else if (baudrate <= 2400)
            count_to = 64;
        else
            count_to = 256;

        for (int i = 0; i < count_to; i++)
        {
            buf0.sputc((char) i );
            buf1.sputc((char) (count_to - 1 - i) );
        }
        buf0.pubsync();
        buf1.pubsync();

        int i0 = 0, i1 = 0;
        int ok0 = 0, ok1 = 0;

        do {
            while (buf0.in_avail() > 0)
            {
                if (buf0.sbumpc() == count_to - 1 - i0)
                    ++ok0;
                ++i0;
            }
            while (buf1.in_avail() > 0)
            {
                if (buf1.sbumpc() == i1)
                    ++ok1;
                ++i1;
            }
#ifdef IOSERIAL_POSIX
            usleep(20'000'000 / baudrate + 10'000);
#elif defined IOSERIAL_WINDOWS
            Sleep(20'000 / baudrate + 10);
#endif
        } while (buf0.in_avail() > 0 || buf1.in_avail() > 0);

        if (ok1 != i1 || i1 != count_to)
            cerr << "FAIL: device 1 received " << i1 << " bytes, " << ok1 <<
               " of them correct, out of " << count_to <<
                " at baudrate " << baudrate << endl;
        else
            cerr << "Testing " << count_to << " bytes at baudrate " << baudrate
                << " from device 0 to device 1 was successful" << endl;

        if (ok0 != i0 || i0 != count_to)
            cerr << "FAIL: device 0 received " << i0 << " bytes, " << ok0 <<
               " of them correct, out of " << count_to <<
                " at baudrate " << baudrate << endl;
        else
            cerr << "Testing " << count_to << " bytes at baudrate " << baudrate
                << " from device 1 to device 0 was successful" << endl;
    }
}

void validate_args(int argc, char* argv[])
{
    if (argc != 3) {
#ifdef IOSERIAL_POSIX
        cerr << "Usage: ioserial.test SERIAL_DEVICE_A SERIAL_DEVICE_B\n\n";
        cerr << "Example:\n$ ./ioserial.test /dev/ttyS0 /dev/ttyUSB1\n";
#elif defined IOSERIAL_WINDOWS
        cerr << "Usage: ioserial.test.exe SERIAL_DEVICE_A SERIAL_DEVICE_B\n\n";
        cerr << "Example:\n> ioserial.test.exe COM1 COM2\n";
#endif
        cerr << "Both ports must be connected with a null-modem link\n";
        cerr << flush;
        exit(-1);
    }

    dev0 = argv[1];
    dev1 = argv[2];

    if (dev0 == dev1)
        throw invalid_argument("The serial ports must be different devices");

    cerr << "Validating serial device " << dev0 << ": ";
    serialbuf buf0;
    buf0.open(dev0, ios_base::out);
    if (!buf0.is_open())
    {
        cerr << "failed\n";
        throw invalid_argument(dev0 + " is not a serial port");
    }
    else
        cerr << "ok\n";

    cerr << "Validating serial device " << dev1 << ": ";
    serialbuf buf1;
    buf1.open(dev1, ios_base::in);
    if (!buf1.is_open())
    {
        cerr << "failed\n";
        throw invalid_argument(dev1 + " is not a serial port");
    }
    else
        cerr << "ok\n";

    // Clear the kernel buffers:
#ifdef IOSERIAL_POSIX
            usleep(500'000);
#elif defined IOSERIAL_WINDOWS
            Sleep(500);
#endif
    buf0.pubsync();
    buf1.pubsync();

    cerr << "Kernel buffers cleared\n";
}

#if 0 // Serial ports
void test_autodetection()
{
    auto ports = ninbot::ioserial::serial_ports();
    cerr << "There are " << ports.size() << " serial ports autodetected:\n";
    for (auto serialport : ports)
    {
        cerr << serialport << "\n";
    }
}
#endif

void test_serialstream()
{
    cerr << "Testing serialtream ...\n";
    serialstream sout(dev0);
    serialstream sin (dev1);
    sout.baudrate(9600);
    sin .baudrate(9600);

    cerr << "Sending data over serial device " << dev0 << "...\n";
    sout << 7357 << " This line starts with 7357"
         << " and was transmitted over serial device " << dev0 << endl;

    int integer;
    sin >> integer;
    string in;
    getline(sin, in);

    if (in.empty())
        cerr << "Timeout error: Data expected on serial device " << dev1
             << " but nothing was received.\n";
    else
        cerr << "Data received on " << dev1 << ": \n>>>" << integer << in << "<<<\n";
}


int main(int argc, char * argv[])
{
#if 0 // Serial ports
    test_autodetection();
#endif
    validate_args(argc, argv);
    test_baudrates();
    // TODO: Test buffer.
    test_serialstream();

    return 0;
}

