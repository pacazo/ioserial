# CLASS REFERENCE

## `basic_serialbuf`

All the classes listed here are declared inside the namespace `nin`.

```cpp
template<
    typename CharT,
    typename Traits = std::char_traits<CharT>
> class basic_serialbuf : public std::basic_streambuf<CharT, Traits>;
```

The class `basic_serialbuf` is a `std::basic_streambuf` that controls input and/or output to a RS-232 serial port.
It has two independent buffers for input and output to optimize accesses. Since serial ports are character devices, the seek functions do nothing.

The serial port is automatically set up and configured for operation with `open()` and closed with `close()` or at object destruction. The return value of `is_open()` indicates that the port was successfully detected, opened and configured. By default, the device is opened at 9600 bps and any potential data in the kernel buffers is cleared.

`basic_serialbuf` is designed to be used with STL streams for formatted I/O, but it also supports standalone use for granular access to the serial port. Putting back characters into the get buffer might fail. Specifically, just after a flush, either automatic or user-triggered, will disable the putback functionality until more characters are read.

Internally, `sync()` blocks until all characters have been sent. This behavior is forced for the sake of consistency amonsgt all serial devices because there are some device drivers that block anyway until all data has been transmitted. Use async for non-blocking behavior, but take into account that `basic_serialbuf` is **not thread safe** and it is **not thread compatible**.

Please check the documentation for `std::basic_streambuf` of your compiler for information about derived member functions.




One specialization is also defined:

|    Type     |       Definition        |
| ----------- | ----------------------- |
| `serialbuf` | `basic_serialbuf<char>` |


### Member types


|Type | Definition|
| --- | --------- |
| `char_type` | `CharT` |
| `traits_type` | `Traits` |
| `int_type` | `Traits::int_type` |
| `pos_type` | `Traits::pos_type` |
| `off_type` | `Traits::off_type` |


### Public member functions

#### `(constructor)`

The default constructor and the move constructor are enabled.
The copy constructor is disabled to prevent multiple buffers accessing the same device file. The move constructor transfers the device ownership and the moved serial buffer becomes closed.

```cpp
basic_serialbuf() = default;
basic_serialbuf(basic_serialbuf const& other) = delete;
basic_serialbuf(basic_serialbuf     && other);
```

#### `(destructor)`

The virtual destructor cleans up the buffers and closes the device file as if `close()` was called.

```cpp
~basic_serialbuf() override;
```

#### `operator = ()`

The copy assignment operator is disabled to prevent multiple buffers accessing the same device file. The move assignment operator transfers the device ownership and the moved serial buffer becomes closed.

**Returns** `*this`.

```cpp
basic_serialbuf & operator = (basic_serialbuf const& rhs) = delete;
basic_serialbuf & operator = (basic_serialbuf     && rhs);
```

#### `swap()`

Exchange ownership of serial device files. Only the device handles are swapped. The internal buffers of both instances are reset.

```cpp
void swap(basic_serialbuf & rhs);
```


#### `is_open()`

Checks that the serial device port was successfully opened and is operating normally.

```cpp
bool is_open() const;
```

#### `open()`

Attempts to open the serial device indicated in `filename`. If succeeds and `filename` is a serial port device file, `open()` sets the device to raw mode, clears the kernel buffers associated to `filename` and sets the baudrate to 9600bps.

If `mode` contains the flag `std::ios_base::in`, `open()` sets up the input buffer and opens the port in read mode. If `mode` contains the flag `std::ios_base::out`, `open()` sets up the output buffer and opens the port in write mode. Both flags may be specified, in which case both input and output buffers are set up and the device file is open in read/write mode. If none of these two flags are present in `mode`, `open()` does nothing. Other flags in `mode` are ignored. The file is always opened in _binary_ mode.

Although it is possible to open two serialbuf on the same device file, the behavior is undefined.


**Returns** `this` if successful. `nullptr` if failed.

```cpp
basic_serialbuf * open(
    std::string const& filename,
    std::ios_base::openmode mode);

```



#### `close()`

Releases the resources reserved by the instance. Specifically, closes the device file and destroys the input and output buffers. All data in the buffers is lost, i.e. data waiting to be flushed to the device is lost.

If the serial device is already closed, `close()` does nothing.

**Returns** `this`.

```cpp
void close() const;
```

#### `baudrate()`

Sets or gets the baudrate.
By default, the baudrate is 9600bps.

```cpp
unsigned int baudrate() const;               (1)
void baudrate(unsigned int new_baudrate);    (2)
```

(1) Retrieves the device setting configuration from the kernel driver and returns the baudrate in bauds.

(2) Sets the device baudrate to `new_baudrate`. If `new_baudrate` is not supported or `baudrate()` fails in some other way, it sends a diagnosis message to `std::cerr`.


In POSIX systems, the supported values for `new_baudrate` are:

```cpp
0
50
75
110
134
150
300
600
1200
1800
2400
4800
9600
19200
38400
57600
115200
230400
```

In Windows systems, the supported values for `new_baudrate` are:

```cpp
110
300
600
1200
2400
4800
9600
14400
19200
38400
57600
115200
128000
256000
```


The serial driver might only support some of these baudrates.



### Non-member functions

#### `swap()`

Provided for compatibility with the STL.

Effectively calls `lhs.swap(rhs)`

```cpp
template <typename CharT, typename Traits>
void swap( basic_serialbuf<CharT, Traits>& lhs,
           basic_serialbuf<CharT, Traits>& rhs );
```



## serialstream

This class adds support for the STL formatting capabilities of streams. It is derived from ```std::basic_iostream<char>```. Once created and opened successfully, serial ports can be used as C++ streams.

One specialization is defined:

|    Type     |       Definition        |
| ----------- | ----------------------- |
| `serialbuf` | `basic_serialbuf<char>` |

### Public member functions

#### `(constructor)`

The default constructor and the move constructor are enabled.
The default constructor creates a serial stream with no associated buffer, that is, in close state.
The copy constructor is disabled to prevent multiple buffers accessing the same device file. The move constructor transfers the device ownership and the moved stream becomes closed.

The last constructor creates a serial stream and then attempts to `open()` the serial device indicated in `filename`. If `open()` fails, an exception is thrown.


```cpp
serialstream() = default;
serialstream(serialstream const& other) = delete;
serialstream(serialstream     && other);
serialstream(std::string  const& filename);
```


#### `(destructor)`

The virtual destructor releases the device file as if `close()` was called.

```cpp
~serialstream();
```

#### `rdbuf()`
**Returns** a pointer to the underlying serial buffer.

```cpp
basic_serialbuf<CharT, Traits> * rdbuf() const;
```

#### `is_open()`
Checks that the serial device was successfully opened and it is
operating normally.

```cpp
bool is_open() const;
```

#### `open()`
Attempts to open the serial device indicated in `filename` for transmitting and receiving.
The device is opened at a baudrate of 9600bps.

**Returns** Nothing. You can check that `open()` succeeded by quering `is_open()`.

```cpp
void open( std::string const& filename );
```

#### `close()`
Close the underlying serial device and release 

```cpp
void close();
```

#### `baudrate()`
Query or change the current baudrate of the underlying serial device.

See the documentation of `basic_serialbuf::baudrate()` for a list of supported baudrate values.

**Returns** Quering returns the current baudrate or `-1` on error. Setting a new baudrate displays a message on stderr in case of error.

```cpp
unsigned int baudrate() const;
void baudrate(unsigned int new_baudrate);
```

### Example 1

```cpp
#include "ioserial.hh"

int main(int argc, char * argv[])
{
    ioserial::serialstream myserialport ("COM1");
    for (int i = 1; i < argc; i++)
        myserialport << argv[i] << std::endl;
    
    myserialport.flush();
    return 0;
}
```

### Example 1

```cpp
#include "ioserial.hh"

int main()
{
    ioserial::serialstream TxRx ("/dev/ttyUSB0");
    std::string command = "Ping";
    std::string reply;

    TxRx << command.length() << command;
    TxRx >> reply;
    
    std::cout << "Device replied >" << reply << "<" << std::endl;
}
```
