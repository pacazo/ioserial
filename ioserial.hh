/* Copyright 2020-2022 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of ioserial.
 *
 * ioserial is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * Ioserial is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ioserial.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef IOSERIAL_HH
#define IOSERIAL_HH

#if defined (__unix__) || (defined (__APPLE__) && defined (__MACH__))
  #define IOSERIAL_POSIX
#elif defined _WIN32 || defined _WIN64
  #define IOSERIAL_WINDOWS
#else
  #error "ioserial: Not POSIX, not Windows: Unknown system!"
#endif

#include <algorithm>
#include <cstddef>
#include <cstdio>
#include <cstring>
#include <filesystem>
#include <iostream>
#include <iterator>
#include <vector>

#ifdef IOSERIAL_POSIX
  #include <termios.h>
  #include <unistd.h>
  #include <poll.h>
  #include <fcntl.h>
  #include <errno.h>
#elif defined IOSERIAL_WINDOWS
  #include <Windows.h>
#endif


namespace ioserial {

#if 0 // Serial ports
std::vector<std::filesystem::path> serial_ports();

std::vector<std::filesystem::path>
serial_ports()
{
#ifdef __linux__
    std::vector<std::filesystem::path> ports;
    ports.push_back("/sys");
    return ports;
#else
    return {};
#endif
}
#endif

/*________________________________ SERIALBUF _________________________________*/

constexpr unsigned int serialbuffer_defaultsize = 4096; // See termios man page
constexpr unsigned int serialbuffer_guaranteed_pbackfail = 16; // TODO

template <typename CharT, typename Traits = std::char_traits<CharT>>
class basic_serialbuf : public std::basic_streambuf<CharT, Traits>
{
    static_assert(sizeof(CharT) == 1);

#ifdef IOSERIAL_POSIX
    int fd = -1; // -1 indicates closed.
#elif defined IOSERIAL_WINDOWS
    HANDLE fd = 0; // 0 indicates closed.
#endif

    std::vector<CharT> ibuffer; // if in,  then ibuffer.size() > 0
    std::vector<CharT> obuffer; // if out, then obuffer.size() > 0
    void setup_bufptr();

public:
    using char_type   = CharT;
    using traits_type = Traits;
    using int_type    = typename traits_type::int_type;
    using pos_type    = typename traits_type::pos_type;
    using off_type    = typename traits_type::off_type;

    basic_serialbuf() = default;
    basic_serialbuf(basic_serialbuf const& copy) = delete;
    basic_serialbuf(basic_serialbuf     && move);

    basic_serialbuf & operator = (basic_serialbuf const& copy) = delete;
    basic_serialbuf & operator = (basic_serialbuf     && move);

    void swap(basic_serialbuf & rhs);

    ~basic_serialbuf() override;

public:
    bool is_open() const;
    basic_serialbuf       (std::string const& filename,
                           std::ios_base::openmode mode);
    basic_serialbuf * open(std::string const& filename,
                           std::ios_base::openmode mode);
    basic_serialbuf * close();

    unsigned int baudrate() const;
    void baudrate(unsigned int new_baudrate);

    // Set to 0 for disabled timeout, i.e. blocking reads.
    std::chrono::microseconds timeout = std::chrono::milliseconds(50);

protected:
    std::streamsize showmanyc() override;
    int_type underflow() override;

    int_type pbackfail(int_type c = traits_type::eof()) override;
    int_type overflow(int_type c = traits_type::eof()) override;

    int sync() override;
};

template <typename CharT, typename Traits>
void swap(basic_serialbuf<CharT, Traits>& lhs,
          basic_serialbuf<CharT, Traits>& rhs)
{
    lhs.swap(rhs);
}

using serialbuf = basic_serialbuf<char>;



/* ***  DEFINITIONS  *** */


template <typename CharT, typename Traits>
void
basic_serialbuf<CharT, Traits>:: setup_bufptr()
{
    if (!ibuffer.empty())
        this->setg(ibuffer.data(), ibuffer.data() + ibuffer.size(),
                                   ibuffer.data() + ibuffer.size());
    else
        this->setg(nullptr, nullptr, nullptr);

    if (!obuffer.empty())
        this->setp(obuffer.data(), obuffer.data() + obuffer.size());
    else
        this->setp(nullptr, nullptr);
}


template <typename CharT, typename Traits>
basic_serialbuf<CharT, Traits>:: basic_serialbuf(basic_serialbuf && move)
{
    *this = std::move(move);
}


template <typename CharT, typename Traits>
basic_serialbuf<CharT, Traits> &
basic_serialbuf<CharT, Traits>:: operator = (basic_serialbuf && move)
{
    if (is_open())
        close();

#ifdef IOSERIAL_POSIX
    fd = std::exchange(move.fd, -1);
#elif defined IOSERIAL_WINDOWS
    fd = std::exchange(move.fd, 0);
#endif

    ibuffer = std::move(move.ibuffer);
    obuffer = std::move(move.obuffer);
    move.setup_bufptr();
    setup_bufptr();

    timeout = move.timeout;

    return *this;
}


template <typename CharT, typename Traits>
basic_serialbuf<CharT, Traits>:: ~basic_serialbuf()
{
    close();
}


template <typename CharT, typename Traits>
void
basic_serialbuf<CharT, Traits>:: swap(basic_serialbuf & rhs)
{
    swap( fd, rhs.fd );
    swap( ibuffer, rhs.ibuffer );
    swap( obuffer, rhs.obuffer );
    rhs.setup_bufptr();
    setup_bufptr();
}


template <typename CharT, typename Traits>
bool
basic_serialbuf<CharT, Traits>:: is_open() const
{
#ifdef IOSERIAL_POSIX
    return fd != -1;
#elif defined IOSERIAL_WINDOWS
    return fd != 0;
#endif
}

template <typename CharT, typename Traits>
basic_serialbuf<CharT, Traits>:: basic_serialbuf(
        std::string const& filename, std::ios_base::openmode mode)
{
    if (!open(filename, mode))
        throw std::runtime_error("Could not open serial port");
}

// Returns nullptr if fails
template <typename CharT, typename Traits>
basic_serialbuf<CharT, Traits> *
basic_serialbuf<CharT, Traits>:: open(std::string const& filename,
                                      std::ios_base::openmode mode)
{
    if (is_open())
        close();

    bool in  = std::ios_base::in  & mode;
    bool out = std::ios_base::out & mode;

#ifdef IOSERIAL_POSIX
    int rw_flag = O_NONBLOCK | O_DSYNC | O_SYNC | O_NOCTTY;
    if ( in && !out) rw_flag |= O_RDONLY;
    if (!in &&  out) rw_flag |= O_WRONLY;
    if ( in &&  out) rw_flag |= O_RDWR;
    if (!in && !out) return nullptr;

    fd = ::open(filename.c_str(), rw_flag);

    if (!is_open())
        return nullptr;
    else if (fd > 1023)
        return nullptr;
        //throw std::runtime_error("ioserial: select cannot handle fd values "
                                 //"greater than 1023");

    termios termios_v;
    if (tcgetattr(fd, &termios_v))
        goto failed;
    cfmakeraw(&termios_v);
    if (tcsetattr(fd, TCSAFLUSH, &termios_v))
    {
    failed:
        close();
        return nullptr;
    }
#endif

#ifdef IOSERIAL_WINDOWS
    std::string portname = "\\\\.\\COM";
    std::copy_if(filename.begin(), filename.end(), std::back_inserter(portname),
            [](char_type c){ return c >= '0' && c <= '9'; });

    DWORD access = 0, share = 0;
    if ( in && !out) access |= GENERIC_READ;  share = FILE_SHARE_WRITE;
    if (!in &&  out) access |= GENERIC_WRITE; share = FILE_SHARE_READ;
    if ( in &&  out) access |= GENERIC_READ | GENERIC_WRITE;
    if (!in && !out) return nullptr;

    fd = CreateFile(portname.c_str(), access, share, nullptr,
                    OPEN_EXISTING, 0, nullptr);
    if (fd == INVALID_HANDLE_VALUE)
    {
        fd = 0;
        return nullptr;
    }

    auto timeout_ms = std::chrono::duration_cast<std::chrono::milliseconds>(this->timeout);
    DWORD readwait = static_cast<DWORD>( timeout_ms.count() );
    COMMTIMEOUTS timeouts = {0, 0, readwait, 0, 0};
    SetCommTimeouts(fd, &timeouts);

    if (in)
        PurgeComm(fd, PURGE_RXABORT | PURGE_RXCLEAR);
#endif

    if (in)  ibuffer.resize(serialbuffer_defaultsize);
    if (out) obuffer.resize(serialbuffer_defaultsize);
    setup_bufptr();

    // Different drivers start with different baudrates (i.e. FTDI USB starts
    // at max 230400; pl2304 starts at 9600). We want an homogeneus
    // behaviour throughout all serial ports.
    baudrate(9600);

    // Some drivers (i.e. FTDI USB) save the kernel buffer between device file
    // accesses. We want it new and clean, so we purge the data.
    while (this->in_avail() > 0)
        this->sbumpc();

    return this;
}


template <typename CharT, typename Traits>
basic_serialbuf<CharT, Traits> *
basic_serialbuf<CharT, Traits>:: close()
{
    if (is_open())
    {
#ifdef IOSERIAL_POSIX
        ::close(fd);
        fd = -1;
#elif defined IOSERIAL_WINDOWS
        CloseHandle(fd);
        fd = 0;
#endif
        ibuffer.clear();
        obuffer.clear();
        setup_bufptr();
    }
    return this;
}


#ifdef IOSERIAL_POSIX
inline std::vector<std::pair<speed_t, unsigned>> baudrates = {
    { B0,      0      },
    { B50,     50     },
    { B75,     75     },
    { B110,    110    },
    { B134,    134    },
    { B150,    150    },
    { B300,    300    },
    { B600,    600    },
    { B1200,   1200   },
    { B1800,   1800   },
    { B2400,   2400   },
    { B4800,   4800   },
    { B9600,   9600   },
    { B19200,  19200  },
    { B38400,  38400  },
    { B57600,  57600  },
    { B115200, 115200 },
    { B230400, 230400 }
};
#endif

#ifdef IOSERIAL_WINDOWS
inline std::vector<std::pair<DWORD, unsigned>> baudrates = {
    { CBR_110,    110    },
    { CBR_300,    300    },
    { CBR_600,    600    },
    { CBR_1200,   1200   },
    { CBR_2400,   2400   },
    { CBR_4800,   4800   },
    { CBR_9600,   9600   },
    { CBR_14400,  14400  },
    { CBR_19200,  19200  },
    { CBR_38400,  38400  },
    { CBR_57600,  57600  },
    { CBR_115200, 115200 },
    { CBR_128000, 128000 },
    { CBR_256000, 256000 },
};
#endif


template <typename CharT, typename Traits>
unsigned int
basic_serialbuf<CharT, Traits>:: baudrate() const
{
    if (!is_open())
        return -1;

#ifdef IOSERIAL_POSIX
    termios termios_v;
    speed_t baudrate;
    if (tcgetattr(fd, &termios_v)) {
        return -1;
    }
    if (ibuffer.size()) baudrate = cfgetispeed(&termios_v);
    if (obuffer.size()) baudrate = cfgetospeed(&termios_v);
#elif defined IOSERIAL_WINDOWS
    DCB dcb = { 0 };
    dcb.DCBlength = sizeof(DCB);

    if (!GetCommState(fd, &dcb))
    {
        std::cerr << "ioserial: GetCommState() failed" << std::endl;
        return -1;
    }
    DWORD baudrate = dcb.BaudRate;
#endif

    auto it = std::find_if(baudrates.begin(), baudrates.end(),
            [baudrate](auto & p) { return p.first == baudrate; });

    return (it == baudrates.end()) ? -1 : it->second;
}


template <typename CharT, typename Traits>
void
basic_serialbuf<CharT, Traits>:: baudrate(unsigned int bps)
{
    if (!is_open())
        return;

    sync();

    auto it = std::find_if(baudrates.begin(), baudrates.end(),
            [bps](auto & p) { return p.second == bps; } );
    if (it == baudrates.end() || bps == 0)
    {
        std::cerr << "ioserial: Unknown baudrate " << bps << std::endl;
        return;
    }

#ifdef IOSERIAL_POSIX
    termios termios_v;

    if ( (tcgetattr(fd, &termios_v))

    || (   (ibuffer.size() && cfsetispeed(&termios_v, it->first))
        || (obuffer.size() && cfsetospeed(&termios_v, it->first)) )

    || (tcsetattr(fd, TCSAFLUSH, &termios_v))

    || baudrate() != bps)
    {
        std::cerr << "ioserial: Baudrate change failed" << std::endl;
    }
#elif defined IOSERIAL_WINDOWS
    DCB dcb = { 0 };
    dcb.DCBlength = sizeof(DCB);

    if (!GetCommState(fd, &dcb))
    {
        std::cerr << "ioserial: GetCommState() failed" << std::endl;
        return;
    }
    dcb.BaudRate = it->first;
    dcb.fParity = false;
    dcb.fOutxCtsFlow = false;
    dcb.fOutxDsrFlow = false;
    dcb.fDtrControl = DTR_CONTROL_DISABLE;
    dcb.fDsrSensitivity = false;
    dcb.fOutX = false;
    dcb.fInX = false;
    dcb.fNull = false;
    dcb.fRtsControl = RTS_CONTROL_DISABLE;
    dcb.fAbortOnError = false;
    dcb.ByteSize = 8;
    dcb.Parity = NOPARITY;
    dcb.StopBits = ONESTOPBIT;

    if (!SetCommState(fd, &dcb))
    {
        std::cerr << "ioserial: SetCommState() failed" << std::endl;
        return;
    }

    if (baudrate() != it->first)
        std::cerr << "ioserial: Baudrate change failed" << std::endl;
#endif
}


template <typename CharT, typename Traits>
std::streamsize
basic_serialbuf<CharT, Traits>:: showmanyc()
{
    if (!is_open() || ibuffer.empty()) // Empty means buffer is not allocated, so READ is disabled.
        return -1;

#ifdef IOSERIAL_POSIX
    pollfd pfd_v = {fd, POLLIN, 0};
    int pollret = poll(&pfd_v, 1, 0); // timeout = 0: Non-blocking

    if (pollret == -1)
        throw std::runtime_error(std::string{"ioserial.showmanyc():poll() "}
                + strerror(errno));
    else if (pollret == 0)
        return -1;
    else // pollret must be 1 (One file with read data), hence we have data.
        return 1;
#elif defined IOSERIAL_WINDOWS
    COMSTAT stat;
    ClearCommError(fd, nullptr, &stat);
    return (stat.cbInQue > 0) ? 1 : -1;
#endif
}


template <typename CharT, typename Traits>
typename basic_serialbuf<CharT, Traits>::int_type
basic_serialbuf<CharT, Traits>::underflow()
{
    if (!is_open() || ibuffer.empty())
        return traits_type::eof();

#ifdef IOSERIAL_POSIX
    ::fd_set fdset;
    FD_ZERO(&fdset);
    FD_SET(fd, &fdset);
    auto timeout_us = std::chrono::duration_cast<std::chrono::microseconds>(this->timeout);
    timeval ttimeout = { timeout_us.count() / 1'000'000,
                         timeout_us.count() % 1'000'000 };

    int selectresult = ::select(fd + 1, &fdset, nullptr, nullptr, &ttimeout);
    if (selectresult != 1)
        return traits_type::eof();
#endif

    constexpr size_t csize = sizeof(char_type);
    const size_t glen = this->egptr() - this->gptr();
    // If no new bytes are read, the effect of the function is relocating
    // the data to the start of the read buffer.
    ::memmove(ibuffer.data(), this->gptr(), glen * csize);

    char_type* buf_start = this->eback() + glen;
    size_t     buf_free  = (ibuffer.size() - glen) * csize;

#ifdef IOSERIAL_POSIX
    const ssize_t cread = ::read(fd, buf_start, buf_free);
#elif defined IOSERIAL_WINDOWS
    DWORD wread = 0;

    bool read_result = ReadFile(fd, buf_start, static_cast<DWORD>(buf_free),
                                &wread, nullptr);

    size_t cread = wread;
#endif

    this->setg(ibuffer.data(), ibuffer.data(),
               ibuffer.data() + glen + cread /* FIXME: (cread / csize) */ );

    if (cread > 0)
        return traits_type::to_int_type(ibuffer[glen]);
    else
        return traits_type::eof();
}


template <typename CharT, typename Traits>
typename basic_serialbuf<CharT, Traits>::int_type
basic_serialbuf<CharT, Traits>:: pbackfail(int_type c)
{
    throw std::runtime_error("ioserial: pbackfail is not implemented yet");
    //throw std::runtime_error("ioserial: Magic code could not be restored "
                             //"in buffer");
    //if (!is_open() || ibuffer.empty())
        //{ }
    return traits_type::eof(); // TODO
}


template <typename CharT, typename Traits>
typename basic_serialbuf<CharT, Traits>::int_type
basic_serialbuf<CharT, Traits>:: overflow(int_type c)
{
    if (!is_open() || obuffer.empty())
        return traits_type::eof();

    constexpr size_t csize = sizeof(char_type);
    const size_t plen = this->pptr() - this->pbase();
    if (plen == 0)
        return traits_type::eof();
#ifdef IOSERIAL_POSIX
    const ssize_t cwrite = ::write(fd, obuffer.data(), plen * csize);

    if (cwrite == -1)
        return traits_type::eof();
    if (cwrite == 0)
        return traits_type::eof();
    tcdrain(fd);
#elif defined IOSERIAL_WINDOWS
    DWORD wwrite = 0;

    bool write_result = WriteFile(fd, obuffer.data(),
        static_cast<DWORD>(plen * csize), &wwrite, nullptr);

    size_t cwrite = wwrite;
    if (cwrite == 0 || write_result == false)
        return traits_type::eof();
#endif
    size_t new_plen = plen - cwrite;
    memmove(obuffer.data(), obuffer.data() + cwrite, new_plen * csize);

    this->setp(obuffer.data() + new_plen, obuffer.data() + obuffer.size());
    if (traits_type::eq_int_type(c, traits_type::eof()) != true)
    {
        obuffer[new_plen] = static_cast<char_type>(c);
        this->pbump(1);
    }

    return static_cast<int>(cwrite);
}


template <typename CharT, typename Traits>
int
basic_serialbuf<CharT, Traits>:: sync()
{
    if (!is_open())
        return -1;

    if (!obuffer.empty() && this->pbase() != this->pptr())
        overflow();
    if (!ibuffer.empty() && showmanyc() > 0)
        underflow();

    return 0;
}


/*______________________________ SERIALSTREAM _______________________________*/

template < typename CharT,
           typename Traits = std::char_traits<CharT> >
class basic_serialstream : public std::basic_iostream<CharT, Traits>
{
    basic_serialbuf<CharT, Traits> buffer;

public:
    basic_serialstream()
      : std::basic_iostream<CharT, Traits> { &buffer }
    { }

    basic_serialstream(basic_serialstream const& copy) = delete;
    basic_serialstream(basic_serialstream     && move)
      : std::basic_iostream<CharT, Traits> { &buffer }
      , buffer { std::move(move.buffer) }
    { }

    basic_serialstream & operator = (basic_serialstream const& copy) = delete;
    basic_serialstream & operator = (basic_serialstream     && move) {
        buffer = std::move(move.buffer);
    }

    basic_serialstream( std::string const& filename )
      : std::basic_iostream<CharT, Traits> { &buffer }
      , buffer {filename, std::ios_base::in | std::ios_base::out}
    { }


public:
    basic_serialbuf<CharT, Traits> * rdbuf() const {
        return &buffer;
    }

    bool is_open() const {
        return buffer.is_open();
    }

    void open( std::string const& filename ) {
        return buffer.open(filename, std::ios_base::in | std::ios_base::out);
    }

    void close() {
        buffer.close();
    }

    unsigned int baudrate() const {
        return buffer.baudrate();
    }

    void baudrate(unsigned int new_baudrate) {
        buffer.baudrate(new_baudrate);
    }
};

template < typename CharT,
           typename Traits>
void swap(basic_serialstream<CharT, Traits>& lhs,
          basic_serialstream<CharT, Traits>& rhs)
{
    std::swap(lhs, rhs);
}

using serialstream  = basic_serialstream< char >;


} // namespace ioserial
#endif

